<?php namespace App\Http\Controllers;

use App\Article;
use App\Http\Requests;
use App\Http\Requests\ArticleRequest;

use App\Picture;
use App\Tag;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ArticlesController extends Controller {

    /**
     * Make all views except index and show only accessible for logged in users
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index', 'show']]);
    }

    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        //Fetch latest articles in descending order
        //Use scope (published) to filter only published articles
        $articles = Article::latest('published_at')->published()->get();

        return view('articles.index', compact('articles'));
    }

    /**
     * @param $article
     * @return \Illuminate\View\View
     */
    public function show(Article $article)
    {
        return view('articles.show', compact('article'));
    }

    /**
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $tags = Tag::lists('name', 'id');
        return view('articles.create', compact('tags'));
    }

    /**
     * @param ArticleRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(ArticleRequest $request)
    {
        $article = new Article($request->all()); // Create a new article object and pass on the fields filled in by the user
        Auth::user()->articles()->save($article); // Get the logged in users' articles and save the new one to the collection
        $this->syncTags($article, $request->input('tag_list')); //Attach the tags to the article
        if ($request->hasFile('file')) { // If the user has attached a file
            $file = $request->file('file'); // Save the file in a variable
            $nameOfPic = $file->getClientOriginalName(); // Save the name and mime type of the file in a variable
            $mime = $file->getMimeType();
            $picture = new Picture; // New instance of the model Picture
            $picture->url = $nameOfPic; // Set url on the picture object
            $picture->mimeType = $mime; // Set mime Type for the picture object
            $picture->article_id = $article->id; // Set foreign key
            $picture->save(); // Save picture to the database
            $file->move('uploads/', $file->getClientOriginalName()); // Move the file to folder 'uploads'
        }
        session()->flash('flash_message', 'Your post has been created!'); //Send notification to the session
        // Redirect the user to all articles with flash message and importance indicator
        flash()->success('Your post has been created!');
        return redirect('articles');
    }

    /**
     * @param Article $article
     * @return \Illuminate\View\View
     * @internal param $id
     */
    public function edit(Article $article)
    {
        $tags = Tag::lists('name', 'id');
        return view('articles.edit', compact('article', 'tags'));
    }

    /**
     * @param Article $article
     * @param ArticleRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @internal param $id
     */
    public function update(Article $article, ArticleRequest $request)
    {
        $article -> update($request->all()); //Update the article with all filled in inputs
        $this->syncTags($article, $request->input('tag_list'));
        return redirect('articles');
    }

    /**
     * Sync up the list of tags in the database
     * @param Article $article
     * @param ArticleRequest $request
     */
    private function syncTags(Article $article, array $tags)
    {
        $article->tags()->sync($tags); //Sync the tags to the article
    }

    private function syncPicture(Article $article, array $files)
    {
        $article->tags()->sync($files); //Sync the tags to the article
    }

}
