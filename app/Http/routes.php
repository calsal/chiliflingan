<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Whenever the user enters an URL, a given controller handles what should be shown.
Route::get('/', 'ArticlesController@index');
Route::get('login', 'HomeController@index');
Route::get('home', 'HomeController@index');

Route::resource('articles', 'ArticlesController');

Route::get('tags/{tags}', 'TagsController@show');
Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);
