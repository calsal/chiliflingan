<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Picture extends Model {

    protected $fillable = [
        'id',
        'url',
        'mimeType',
        'post_id'
    ];

    /**
     * Returns the article associated with the picture
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function article()
    {
        return $this->hasOne('Post');
    }
}
