<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePicturesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pictures', function(Blueprint $table)
		{
            $table->increments('id'); //Automatically create an id for the Picture object
            $table->string('url');
            $table->string('mimeType');
            $table->timestamps();
            $table->integer('article_id')->unsigned(); //Creates column for foreign key
            $table->foreign('article_id')
                ->references('id')
                ->on('articles')
                ->onDelete('cascade'); //Creates a foreign key for the id of the picture to the id of the article
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pictures');
	}

}
