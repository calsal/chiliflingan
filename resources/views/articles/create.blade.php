@extends('app')

@section('content')

    <h1>Create a new article</h1>

    <hr/>

    {!! Form::model($article = new \App\Article, ['url' => 'articles', 'enctype' => 'multipart/form-data']) !!}

    @include('articles._form', ['submitButtonText' => 'Add Article'])

    {!! Form::close() !!}

    <!-- Validate and check for errors -->
    @include('errors.list')

@endsection
