@extends('app')

@section('content')
    <h1 class="centeredH1">Articles</h1>
    @foreach($articles as $article)
        <article>
            <hr/>
            <a href="{{ action('ArticlesController@show', [$article->id]) }}"><h2>{{ $article->title }}</h2></a>
            {{--If the user is authenticated, show edit option --}}
            @if (Auth::check())
                <a href="{{ action('ArticlesController@edit', [$article->id]) }}" class="pull-right">Edit article</a>
            @endif
            @if(!is_null($article->picture()->first()))
                <img src='\uploads/{{$article->picture()->first()->url}}' height="50px"></img>
                <br/><br/>
            @endif
            <div class="body">{{ $article->body }}</div>
        </article>
    @endforeach
@endsection