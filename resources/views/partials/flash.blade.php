{{-- If something is flashed to the session it is displayed --}}
@if(Session::has('flash_message'))
    <div class="alert alert-success {{ Session::has('flash_message_important') ? 'alert_important' : '' }}"> {{-- If flash_message_important exists a close button is visible --}}
        @if(Session::has('flash_message_important'))
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        @endif
        {{ session('flash_message') }}
    </div>
@endif