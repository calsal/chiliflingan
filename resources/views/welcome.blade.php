@extends('app')

@section('content')
    <section class="banner">
        <div class="content">
            <div class="title">chiliflingan</div>
            <button class="loginButton" type="button"><a href="login"><span class="glyphicon glyphicon-log-in" aria-hidden="true"></span> Login</a></button>
            <div class="quote">{{ Inspiring::quote() }}</div>
        </div>
    </section>
@endsection
